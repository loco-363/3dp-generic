/*
From GitHub Gist
https://gist.github.com/Stemer114/324c382a528d10b43759
*/


/*
 * Nut trap
 * default values are for M3 nut
 * 
 * If you use this module as a nut trap cutout, you need to add +de (ie 0.1mm or similiar) to height
 * in order to get non-manifold cutouts
 */
module nut_trap(w = 5.5, h = 3){
	cylinder(r = w / 2 / cos(180 / 6) + 0.05, h=h, $fn=6);
} // nut_trap


nut_trap();
