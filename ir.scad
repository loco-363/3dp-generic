/**
***********************************************
***    Loco363 - 3D Print - Generic code    ***
***     IR LED / Fototransistor library     ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * Package pins (flat)
 * @param h - length of pins
 */
module ir_pins(h){
	for(i = [-1 : 2 : 1]){
		translate([0.35, i*1.27, 0])
			cylinder(d=1.2, h=h);
	}
} // ir_pins


/**
 * Flat package
 */
module ir_flat(){
	difference(){
		translate([-1.5, -3.5, 0])
			cube([3, 7, 7]);
		
		translate([-0.5, -2.4, 1])
			cube([1.7, 4.8, 6+1]);
		
		translate([1, -2.4, 1+3])
			cube([2, 4.8, 3+1]);
		
		translate([0, 0, -1])
			ir_pins(3);
	}
} // ir_flat


/**
 * Round 5mm package
 * @param hB - additional height at the bottom of the package
 *   (e.g. to cut out a hole for insertion of LED/fototransistor)
 * @param hT - additional height at the top of the package
 */
module ir_5mm(hB=1, hT=0){
	// bottom ring
	difference(){
		translate([0, 0, -hB])
			cylinder(d=5.8+0.3, h=1+hB);
			
		translate([-(1+0.2)-(5+0.4)/2, -(1+(5.8+0.3)+1)/2, -hB-1])
			cube([1+0.2, 1+(5.8+0.3)+1, 1+(1+hB)+1]);
	}
	
	// body
	cylinder(d=5+0.3, h=6+hT);
	
	// top
	translate([0, 0, 6])
		sphere(d=5+0.3);
} // ir_5mm
